﻿using System.Windows.Controls;

namespace VECTO3GUI2020.Views.JobEditViews.Vehicle.Components
{
    /// <summary>
    /// Interaction logic for AxleGearView_v2_0.xaml
    /// </summary>
    public partial class AxleGearView_v2_0 : UserControl
    {
        public AxleGearView_v2_0()
        {
            InitializeComponent();
        }
    }
}
