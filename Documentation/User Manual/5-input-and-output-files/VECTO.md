## Job File

File for the definition of an job in VECTO. A job contains everything what is needed to run a simulation. Can be created with the [Job Editor](#job-editor).

- File format is [JSON](#json).
- Filetype ending is ".vecto"

Refers to other files:

* [Vehicle (VVEH)](#vehicle-file-.vveh)
* [Engine (VENG)](#engine-file-.veng)
* [Gearbox (VGBX)](#gearbox-file-.vgbx)
* [Driving Cycle (VDRI)](#driving-cycles-.vdri)
* [Acceleration Limiting (VACC)](#acceleration-limiting-input-file-.vacc)


**Example:**

~~~json
{
  "Header": {
    "CreatedBy": "",
    "Date": "2020-09-07T15:36:16.4539236Z",
    "AppVersion": "3",
    "FileVersion": 8
  },
  "Body": {
    "SavedInDeclMode": false,
    "EngineOnlyMode": false,
    "VehicleFile": "Group5_HEV.vveh",
    "EngineFile": "Engine_325kW_12.7l.veng",
    "GearboxFile": "AMT_12.vgbx",
    "TCU": "AMT_12.vgbx",
    "ShiftStrategy": "TUGraz.VectoCore.Models.SimulationComponent.Impl.AMTShiftStrategy",
    "HybridStrategyParams": "Hybrid_Parameters.vhctl",
    "AuxiliaryAssembly": "Classic",
    "AuxiliaryVersion": "CLASSIC",
    "AdvancedAuxiliaryFilePath": "",
    "Aux": [],
    "Padd": 5000.0,
    "Padd_electric": 0.0,
    "VACC": "Truck.vacc",
    "EngineStopStartAtVehicleStopThreshold": 2.0,
    "EngineStopStartMaxOffTimespan": 120.0,
    "EngineStopStartUtilityFactor": 1.0,
    "EcoRollMinSpeed": 0.0,
    "EcoRollActivationDelay": 0.0,
    "EcoRollUnderspeedThreshold": 0.0,
    "EcoRollMaxAcceleration": 0.0,
    "PCCEnableSpeed": 0.0,
    "PCCMinSpeed": 0.0,
    "PCCUnderspeed": 0.0,
    "PCCOverSpeed": 5.0,
    "PCCPreviewDistanceUC1": 0.0,
    "PCCPreviewDistanceUC2": 0.0,
    "LAC": {
      "Enabled": true,
      "PreviewDistanceFactor": 10.0,
      "DF_offset": 2.5,
      "DF_scaling": 1.5,
      "DF_targetSpeedLookup": "",
      "Df_velocityDropLookup": "",
      "MinSpeed": 50.0
    },
    "OverSpeedEcoRoll": {
      "Mode": "Overspeed",
      "MinSpeed": 50.0,
      "OverSpeed": 2.5
    },
    "Cycles": [
      "LongHaul.vdri",
      "RegionalDelivery.vdri",
      "UrbanDelivery.vdri"
    ]
  }
}
~~~

