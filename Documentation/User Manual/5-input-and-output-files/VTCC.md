## Torque Converter Characteristics (.vtcc)

The file uses the [VECTO CSV format](#csv).

- Filetype: .vtcc
- Header: **Speed Ratio, Torque Ratio, Input Torque at reference rpm**
- Requires at least 2 data entries


See [Torque Converter Model](#torque-converter-model) for more information about the component model.


**Example:**

~~~
Speed Ratio, Torque Ratio, MP1000
        0.0,         1.93, 377.80
        0.1,         1.82, 365.21
        0.2,         1.70, 352.62
        0.3,         1.60, 340.02
        0.4,         1.49, 327.43
        0.5,         1.39, 314.84
        0.6,         1.28, 302.24
        0.7,         1.18, 264.46
        0.8,         1.07, 226.68
        0.9,         0.97, 188.90
        1.0,         0.97, 0.00
...
~~~
