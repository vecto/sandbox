# Changelog

**VECTO-4.0.0**

***Build 3078 (2023-06-06) RELEASE CANDIDATE***

- First fully functional tool version according to the provisions of the 2nd amendment of Regulation (EU) 2017/2400.   
- Changes
   - Dropped support for .Net Framework 4.5 (EOL 04/2022)
   - Multi-target build. Supported .Net versions: .Net Framework 4.8, .Net 6.0
   - Implementation of Declaration-Mode for xEV-Lorries (see release notes)
   - Implementation of Declaration-Mode for xEV-Buses (see release notes)
   - New generic vehicles (XML)
- Known issues
   - Elements not yet implemented
       - Battery connectors / junction box not included define and implement generic additional resistances (i.e. loss factors) 
       - Technical elements as resulting from the revision of the CO2 Standards to be added
          - Sub-group allocation for the for the newly covered vehicle groups 
          - Generation of weighted results for vocational vehicles
          - Anything related to ZEV definition?
   - Elements still under discussion
      - Medium lorries mission profile and payload weighting factors (equally weighted, only preliminary)
      - Multiple SOC level(s) for VECTO PEV simulation and respective weighting of results (?)


**VECTO-3.3.10**

***Build 2401 (2021-07-29) OFFICIAL RELEASE***

- Bugfixes (compared to 3.3.10.2373)
    * *No additional bugfixes*

***Build 2373 (2021-07-01) RELEASE CANDIDATE***

- Improvements
    * [VECTO-1421] – Added vehicle sub-group (CO2-standards to MRF and CIF)
    * [VECTO 1449] – Handling of exempted vehicles: See next slide for details
    * [VECTO-1404] – Corrected URL for CSS in MRF and CIF
- Bugfixes
    * [VECTO-1419] – Simulation abort in urban cycle: failed to find operating point on search braking power with TC gear
    * [VECTO-1439] – Bugfix handling duplicate entries in engine full-load curve when intersecting with max-torque of gearbox
    * [VECTO-1429] – error in XML schema 2.x for exempted vehicles – MaxNetPower1/2 are optional input parameters

***Handling of exempted vehicles***

- Axle configuration and sleeper cab are optional input parameters for exempted vehicles (XML schema 1.0 and 2.2.1). 
    * OEMs are recommended to provide these parameters for exempted vehicles.
    * If the axle configuration is provided as input parameter, the MRF contains the vehicle group. 
    * The sleeper cab input parameter is also part of the MRF if provided as input.
- Input parameters MaxNetPower1/2 are optional input parameters for all exempted vehicles. 
    * If provided in the input these parameters are part of the MRF for all exempted vehicle types
    * It is recommended that those parameters are used to specify the rated power also for PEV (pure electric vehicles)


**VECTO-3.3.9**

***Build 2175 (2020-12-15) OFFICIAL RELEASE***

- Bugfixes (compared to 3.3.9.2147)
    * [VECTO-1374] - VECTO VTP error - regression update

***Build 2147 (2020-11-17) RELEASE CANDIDATE***

- Bugfixes
    * [VECTO-1331] - VTP Mode does not function for vehicles of group 3
    * [VECTO-1355] - VTP Simulation Abort
    * [VECTO-1356] - PTO Losses not considered in VTP simulation
    * [VECTO-1361] - Torque Converter in use for the First and Second Gear VTP file does not allow for this
    * [VECTO-1372] - Deviation of CdxA Input vs. Output for HDV16
    * [VECTO-1374] - VECTO VTP error

- Improvements
    * [VECTO-1360] - make unit tests execute in parallel


**VECTO-3.3.8**

***Build 2052 (2020-08-14) OFFICIAL RELEASE***

- Bugfixes (compared to 3.3.8.2024)
    * *No additional bugfixes*

***Build 2024 (2020-07-17) RELEASE CANDIDATE***

- Bugfixes 
    * [VECTO-1288] - Simulation Abort UD RL
    * [VECTO-1327] - Simulation abort Construction RefLoad: unexpected response ResponseOverload
    * [VECTO-1266] - Gear 4 Loss-Map was extrapolated


**VECTO 3.3.7**

***Build 1964 (2020-05-18) OFFICIAL RELEASE***

- Bugfixes
    * [VECTO-1254] - Hashing method does not ignore certain XML attributes
    * [VECTO-1259] - Mission profile weighting factors for vehicles of group 16 are not correct


**VECTO 3.3.6**

***Build 1916 (2020-03-31) OFFICIAL RELEASE***

- Bugfixes
    * [VECTO-1250] - Error creating new gearbox file from scratch

***Build 1898 (2020-03-13) RELEASE CANDIDATE***

- Improvement
    * [VECTO-1239] - Adaptation of Mission Profile Weighting Factors
    * [VECTO-1241] - Engineering mode: Adding support for additional PTO activations

- Bugfixes
    * [VECTO-1243] - Bug in VTP mode for heavy lorries
    * [VECTO-1234] - urban cycle at reference load not running for bug when find braking operating point


**VECTO 3.3.5**

***Build 1812 (2019-12-18) OFFICIAL RELEASE***

- Bugfixes
    * [VECTO-1220] - Simulation Abort Urban Delivery RefLoad

***Build 1783 (2019-11-19) RELEASE CANDIDATE***

- Improvement
    * [VECTO-1194] - Handling input parameter 'vocational' for groups other than 4, 5, 9, 10
    * [VECTO-1147] - Updating declaration mode cycles values in user manual 
    * [VECTO-1207] - run VECTO in 64bit mode by default

- Bugfixes
    * [VECTO-1074] - Vecto Calculation Aborts with Interpolation Error
    * [VECTO-1159] - Simulation Abort in UrbanDelivery LowLoading
    * [VECTO-1189] - Error in delaunay triangulation invariant violated
    * [VECTO-1209] - Unexpected Response Response Overload
    * [VECTO-1211] - Simulation Abort Urban Delivery Ref Load
    * [VECTO-1214] - Validation of input data fails when gearbox speed limits are applied


**VECTO 3.3.4**

***Build 1716 (2019-09-13) OFFICIAL RELEASE***

- Bugfixes
    * [VECTO-1074] - Vecto Calculation Aborts with Interpolation Error ([VECTO-1046])
    * [VECTO-1111] - Simulation Abort in Municipal Reference Load


***Build 1686 (2019-08-14) RELEASE CANDIDATE***

- Improvement
    * [VECTO-1042] - Add option to write results into a certain directory
    * [VECTO-1064] - add weighting factors for vehicle groups 1, 2, 3, 11, 12, 16

- Bugfixes
    * [VECTO-1030] - Exceeded max iterations when searching for operating point! Failed to find operating point!
    * [VECTO-1032] - Gear 5 LossMap data was extrapolated in Declaration Mode: range for loss map is not sufficient
    * [VECTO-1067] - Vair and Beta correction for Aerodynamics
    * [VECTO-1000] - Error Loss-Map extrapolation in Declaration Mode
    * [VECTO-1040] - Gear 6 LossMap data was extrapolated in Declaration Mode
    * [VECTO-1047] - Failed to find operating point on construction cycle, ref load, AT gearbox


**VECTO 3.3.3**

***Build 1639 (2019-06-27) OFFICIAL RELEASE***

- Bugfixes (compared to VECTO 3.3.3.1609-RC)
    * [VECTO-1003] - Vecto Error: Loss-Map extrapolation in declaration mode required (issue VECTO-991)
    * [VECTO-1006] - Failed to find torque converter operating point on UD cycle (issue VECTO-996)
    * [VECTO-1010] - Unexpected Response: ResponseOverload in UD cycle (issue VECTO-996)
    * [VECTO-1015] - XML Schema not correctly identified
    * [VECTO-1019] - Error opening job in case a file is missing
    * [VECTO-1020] - HashingTool Crashes
    * [VECTO-1021] - Invalid hash of job data


***Build 1609 (2019-05-29) RELEASE CANDIDATE***

 - Improvement
    * [VECTO-916] - Adding new tyre sizes
    * [VECTO-946] - Refactoring XML reading
    * [VECTO-965] - Add input fields for ADAS into VECTO GUI
    * [VECTO-966] - Allow selecting Tank System for NG engines in GUI
    * [VECTO-932] - Consistency in NA values in the vsum file

 - Bugfixes
    * [VECTO-954] - Failed to find operating point for braking power (Fix for Notification Art. 10(2) - [VECTO-952])
    * [VECTO-979] - VECTO Simulation abort with 8-speed MT transmission (Fix for Notification Art. 10(2) - [VECTO-978])
    * [VECTO-931] - AT error in VECTO version 3.3.2.1519
    * [VECTO-950] - Error when loading Engine Full-load curve
    * [VECTO-967] - Engine-Only mode: Engine Torque reported in .vmod does not match the provided cycle
    * [VECTO-980] - Error during simulation run


**VECTO 3.3.2**

***Build 1548 (2019-03-29) OFFICIAL RELEASE***


 - Bugfixes
    * [VECTO-861] - 3.3.1: Torque converter not working correctly
    * [VECTO-904] - Range for gear loss map not sufficient.
    * [VECTO-909] - 3.3.2.1519: Problems running more than one input .xml
    * [VECTO-917] - TargetVelocity (0.0000) and VehicleVelocity (>0) must be zero when vehicle is halting
    * [VECTO-918] - RegionalDeliveryEMS LowLoading - ResponseSpeedLimitExceeded
    * [VECTO-920] - Urban Delivery: Simulation Run Aborted, TargetVelocity and VehicleVelocity must be zero when vehicle is halting!


***Build 1519 (2019-03-01) RELEASE CANDIDATE***


Release Notes - VECTO: Vehicle Energy Calculation Tool - Version 3.3.2.1519-RC


 - Improvement
    * [VECTO-869] - change new vehicle input fields (ADAS, sleeper cab, etc.) to be mandatory
    * [VECTO-784] - Configuration file for VECTO log files
    * [VECTO-865] - Extend Sum-Data
    * [VECTO-873] - Add digest value to SumData


 - Bugfixes
    * [VECTO-729] - Bugs APT submodel
    * [VECTO-787] - APT: DrivingAction Accelerate after Overload
    * [VECTO-789] - APT: ResponseUnderload
    * [VECTO-797] - VECTO abort with AT transmission and TC table value
    * [VECTO-798] - VECTO abort with certified AT transmission data and certified TC data
    * [VECTO-807] - VECTO errors in vehicle class 1/2/3
    * [VECTO-827] - Torque converter inertia 
    * [VECTO-838] - APT: ResponseOverload
    * [VECTO-843] - AT Transmissions problem on VECTO 3.3.1.1463
    * [VECTO-844] - Error with AT gearbox model
    * [VECTO-847] - Simulation abort due to error in NLog?
    * [VECTO-848] - AT Gearbox Simulation abort (was: Problem related to Tyres?)
    * [VECTO-858] - Urban Delivery Abort - with APT-S Transmission and TC
    * [VECTO-861] - 3.3.1: Torque converter not working correctly
    * [VECTO-872] - MRF/CIF: Torque Converter certification method and certification number not correctly set
    * [VECTO-879] - SIMULATION RUN ABORTED DistanceRun got an unexpected response
    * [VECTO-883] - Traction interruption may be too long
    * [VECTO-815] - Unexpected Response: SpeedLimitExceeded
    * [VECTO-816] - object reference not set to an instance of an object
    * [VECTO-817] - TargetVelocity and VehicleVelocity must not be 0
    * [VECTO-820] - DistanceRun got an unexpected response: ResponseSpeedLimitExceeded
    * [VECTO-864] - Prevent VECTO loss-map extension to result in negative torque loss


**VECTO 3.3.1**

***Build 1492 (2019-02-01) OFFICIAL RELEASE***

 - Bugfixes (compared to 3.3.1.1463)
    * [VECTO-845] - Fixing bug for VECTO-840
    * [VECTO-826] - DistanceRun got an unexpected response: ResponseSpeedLimitExceeded
    * [VECTO-837] - VECTO GUI displays incorrect cycles prior to simulation
    * [VECTO-831] - Addition of indication to be added in Help and Release notes for simulations with LNG
 

***Build 1463 (2019-01-03) RELEASE CANDIDATE***

 - Changes according to 2017/2400 amendments
    * [VECTO-761] - Adaptation of input XML Schema
    * [VECTO-762] - Extension of Input Interfaces
    * [VECTO-763] - Extension of Segmentation Table
    * [VECTO-764] - ADAS benefits
    * [VECTO-766] - Update Powerdemand Auxiliaires
    * [VECTO-767] - Report for exempted vehicles
    * [VECTO-768] - VTP mode
    * [VECTO-770] - Fuel Types
    * [VECTO-771] - Handling of exempted vehicles
    * [VECTO-824] - Throw exception for certain combinations of exempted vehicle parameters
    * [VECTO-773] - Correction Factor for Reference Fuel
    * [VECTO-790] - Adapt generic data for construction/municipal utility
    * [VECTO-493] - Implementation of generic body weights and air drag values for construction cycle
    * [VECTO-565] - Consideration of LNG as possible fuel is missing

 - Changes/Improvements
    * [VECTO-799] - Remove TUG Logos from Simulation Tool, Hashing Tool
    * [VECTO-808] - Add Moitoring Report
    * [VECTO-754] - Extending Loss-Maps in case of AT gearbox for each gear, axlegear, gearbox 
    * [VECTO-757] - Correct contact mail address in Hashing Tool
    * [VECTO-779] - Update Construction Cycle - shorter stop times
    * [VECTO-783] - Rename columns in segmentation table and GUI
    * [VECTO-709] - VTP editor from user manual not matching new VECTO one: updated documentation
    * [VECTO-785] - Handling of Vehicles that cannot reach the cycle's target speed: Limit max speed in driver model
    * [VECTO-716] - Validate data in Settings Tab: update documentation
    * [VECTO-793] - Inconsistency between GUI, Help and Regulation: update wording in GUI and user manual
    * [VECTO-796] - Adaptation of FuelProperties
    * [VECTO-806] - extend loss-maps (gbx, axl, angl) for MT and AMT transmissions
    * [VECTO-750] - Simulation error DrivingAction: adapt downshift rules for AT to drive over hill with 6% inclination

 - Bugfixes
    * [VECTO-819] - object reference not set to an instance of an object
    * [VECTO-818] - SearchOperatingPoint: Unknown response type. ResponseOverload
    * [VECTO-813] - Error "Infinity [] is not allowed for SI-Value"
    * [VECTO-769] - DrivingAction Brake: request failed after braking power was found.ResponseEngineSpeedTooHigh
    * [VECTO-804] - Error on simulation with VECTO 3.3.0.1433
    * [VECTO-805] - Total vehicle mass exceeds TPMLM
    * [VECTO-811] - AMT: ResponseGearShift
    * [VECTO-812] - AMT: ResponseOverload
    * [VECTO-822] - SIMULATION RUN ABORTED by Infinity
    * [VECTO-792] - Vecto Hashing Tool - error object reference not set to an instance of an object (overwriting Date element)
    * [VECTO-696] - Problem with Primary Retarder: regression update, set torque loss to 0 for 0 speed and engaged gear
    * [VECTO-776] - Decision Factor (DF)  field is emptied after each simulation
    * [VECTO-814] - Error: DistanceRun got an unexpected response: ResponseGearshift


**VECTO 3.3.0**

***Build 1433 (2018-12-03) OFFICIAL RELEASE***

- Bugfixes (compared to 3.3.0.1250)
    * [VECTO-723] - Simulation aborts with engine speed too high in RD cycle
    * [VECTO-724] - Simulation aborts with error 'EngineSpeedTooHigh' - duplicate of VECTO-744
    * [VECTO-728] - Simulation aborts when vehicle's max speed (n95h) is below the target speed
    * [VECTO-730] - Simulation Aborts with ResponseOverload
    * [VECTO-744] - ResponseEngineSpeedTooHigh (due to torque limits in gearbox)
    * [VECTO-731] - Case Mismatch - Torque Converter
    * [VECTO-711] - Elements without types in CIF and MRF
    * [VECTO-757] - Correct contact mail address in Hashing Tool
    * [VECTO-703] - PTO output in MRF file
    * [VECTO-713] - Manufacturer Information File in the legislation is not compatible with the Simulation results
    * [VECTO-704] - Allow VTP-simulations for AT gearboxes
- Changes (compared to 3.3.0.1398)
    * [VECTO-795] - VECTO Hashing Tool crashes
    * [VECTO-802] - Error in XML schema for manufacturer's record file


***Build 1398 (2018-10-30) RELEASE CANDIDATE***

- Bugfixes (since 3.3.0.1250)
    * [VECTO-723] - Simulation aborts with engine speed too high in RD cycle
    * [VECTO-724] - Simulation aborts with error 'EngineSpeedTooHigh' - duplicate of VECTO-744
    * [VECTO-728] - Simulation aborts when vehicle's max speed (n95h) is below the target speed
    * [VECTO-730] - Simulation Aborts with ResponseOverload
    * [VECTO-744] - ResponseEngineSpeedTooHigh (due to torque limits in gearbox)
    * [VECTO-731] - Case Mismatch - Torque Converter
    * [VECTO-711] - Elements without types in CIF and MRF
    * [VECTO-757] - Correct contact mail address in Hashing Tool
    * [VECTO-703] - PTO output in MRF file
    * [VECTO-713] - Manufacturer Information File in the legislation is not compatible with the Simulation results
    * [VECTO-704] - Allow VTP-simulations for AT gearboxes


***Build 1250 (2018-06-04)***


- Improvement
    * [VECTO-665] - Adding style information to XML Reports
    * [VECTO-669] - Group 1 vehicle comprises vehicles with gross vehicle weight > 7.5t
    * [VECTO-672] - Keep manual choice for "Validate data"
    * [VECTO-682] - VTP Simulation in declaration mode
    * [VECTO-652] - VTP: Check Cycle matches simulation mode
    * [VECTO-683] - VTP: Quality and plausibility checks for recorded data from VTP
    * [VECTO-685] - VTP Programming of standard VECTO VTP report
    * [VECTO-689] - Additional Tyre sizes
    * [VECTO-702] - Hashing tool: adapt warnings
    * [VECTO-667] - Removing NCV Correction Factor
    * [VECTO-679] - Engine n95h computation gives wrong (too high) engine speed (above measured FLD, n70h)
    * [VECTO-693] - extend vehicle performance in manufacturer record

- Bugfixes
    * [VECTO-656] - Distance computation in vsum
    * [VECTO-666] - CF_RegPer no effect in vehicle simulation -- added to the engine correction factors
    * [VECTO-687] - Saving a Engine-Only Job is not possible
    * [VECTO-695] - Bug in vectocmd.exe - process does not terminate
    * [VECTO-699] - Output in manufacturer report and customer report (VECTO) uses different units than described in legislation
    * [VECTO-700] - errorr in simulation with 0 stop time at the beginning of the cycle 


**VECTO 3.2.1**

***Build 1133 (2018-02-07)***

- Improvement
    * [VECTO-634] - VTP Mode: specific fuel consumption

- Bugfixes
    * [VECTO-642] - VECTO BUG – secondary retarder losses: **IMPORTANT:** Fuel-consumption relevant bug! wrong calculation of retarder losses for retarder ratio not equal to 1
    * [VECTO-624] - Crash w/o comment: Infinite recursion
    * [VECTO-627] - Cannot open Engine-Only Job
    * [VECTO-629] - Vecto crashes without errror message (same issue as VECTO-624)
    * [VECTO-639] - Failed to find operating point for braking power: cycle with low target speed (3km/h). allow driving with slipping clutch
    * [VECTO-640] - Exceeded max. iterations: driving fully-loaded vehicle steep uphill. fixed by allowing full-stop and drive off again
    * [VECTO-633] - unable to start VTP Mode simulation
    * [VECTO-645] - Encountered error while validating Vecto output (generated by API) through Hashing tool for vehicle without retarder
   


***Build 1079 (2017-12-15)***

- Improvements
    * [VECTO-618] - Add Hash value of tyres to manufacturer's record file
    * [VECTO-590] - Handling of hash values: customer's record contains hash of manufacturer's record
    * [VECTO-612] - Continuously changing hashes: Info in GUI of HashingTool
    * [VECTO-560] - Change Mail-Address of general VECTO contact
    * [VECTO-616] - SI-Unit - display derived unit instead of base units

- Bugfixes
    * [VECTO-608] - Power balance in EPT-mode not closed
    * [VECTO-611] - Invalid input. Cannot cast Newtonsoft.Json.Linq.JObject to Newtonsoft.Json.Linq.JToken
    * [VECTO-610] - TyreCertificationNumber missing in Manufacturer Report
    * [VECTO-613] - Incomplete description of allowed values of LegislativeClass (p251) in VECTO parameter documentation
    * [VECTO-625] - Update XML Schema: Tyre dimensions according to Technicall Annex, trailing spaces in enums

- Support
    * [VECTO-615] - Error torque interpolation in declaration jobs exported to XML


***Build 1054 (2017-11-20)***

- Improvements
    + [VECTO-592] - NEW VTP Simulation Mode
    + [VECTO-605] - Improve simulation speed

- Bugfixes
    + [VECTO-602] - Error in simulation without airdrag component
    + [VECTO-589] - Scheme .xml error


**VECTO 3.2.0**

***Build 1022 (2017-10-19)***

- Bugfixes
    + [VECTO-585, VECTO-587] – VECTO Simulation aborts when run as WCF Service
    + [VECTO-586] – Gearshiftcout in reports too high
    + [VECTO-573] – Use of old library references .net framework 2.0


***Build 1005 (2017-10-01)***

- Improvements
    + Release of *VECTO Hashing Tool*

- Bugfixes
    + [VECTO-569] - ‘Engine Retarder’ not correctly recognized as input
    + [VECTO-571] - Customer Report – wrong output format of average RRC
    + [VECTO-573] - Correction of displayed units in graph window
    + [VECTO-575] - Correction of simulation aborts (due to gearbox inertia, engineering mode)
    + [VECTO-577] - Correction of XML export functionality
    + [VECTO-579] - Bug fix GUI crashes on invalid input
    + [VECTO-558] - Correction of output in .vsum file – BFColdHot always 0
    + [VECTO-564] - Bug fix: correct output of vehicle group in XML report
    + [VECTO-566] - Vehicle height not correctly read (engineering mode)
    + [VECTO-545] - Update documentation on Settings dialog

	
***Build 940 (2017-07-28)***

- Bugfixes:
    + [VECTO-546] - GearboxCertificationOptionType Option 2 not accepted by VECTO
    + [VECTO-547] - Engine Manufacturer and Engine Model are empty in .vsum 
    + [VECTO-548] - online user manual
    + [VECTO-549] - Inconsistent (and wrong) decimal separator in XML output (manufacturer report)
    + [VECTO-551] - Average Tyre RRC not in Customer Information File output
    + [VECTO-536] - GUI: improvements vehicle dialog (add missing pictures for vehicle categories)
    + [VECTO-550] - Allow custom settings for AirDensity in Engineering mode
    + [VECTO-552] - set engine rated power, rated speed to computed values from FLD if not provided as input


***Build 925 (2017-07-13)***

- Improvements
    + [VECTO-366] added EMS vehicle configuration, EMS is only simulated when engine rated power > 300kW
    + [VECTO-463] add pneumatic system technology 'vacuum pump'
    + [VECTO-465] change RRC value of trailers (declaration mode) from 0.00555 to 0.0055 (due to limits in user interface)
    + [VECTO-477] AT Gearbox, powershift losses: remove inertia factor
    + [VECTO-471] update cross-wind correction model: height-dependent wind speed (see Excel spreadsheet in User Manual folder for details)
    + [VECTO-367] Add Vehicle Design Speed to segmentation table
    + [VECTO-470] Add XML reading and export functionality
    + [VECTO-486] Adding hashing library
    + [VECTO-469] Limit engine max torque (either due to vehicle or gearbox limits), limit gearbox input speed
    + [VECTO-466] Update vehicle payloads: 10% loaded and reference load are simulated
    + [VECTO-467] Add generic PTO activation in municipal cycle
    + [VECTO-468] Add PTO losses (idle) in declaration mode
    + [VECTO-479] Added PTO option 'only one engaged gearwheel above oil level' with 0 losses
    + [VECTO-483] Adapt CdxA supplement for additional trailers
    + [VECTO-494] Implementation of different fuel types
    + [VECTO-502] Implementing standard values for air-drag area (if not measured)
    + [VECTO-501] Implement engine idle speed set in vehicle (must be higher than engine's idle speed value)
    + [VECTO-504] Adding HVAC technology 'none'
    + [VECTO-489] Extrapolate gearbox lossmaps (required when torque limitation by gearbox is ignored)
    + [VECTO-505] Implement AT transmissions in declaration mode
    + [VECTO-507] Allow to ignore validation of model data when starting a simulation (significant improvement on simulation startup time - about 10s)
    + [VECTO-506] modified method how torque-converter characteristics in drag is extended. allow drag-values in the input, only add one point at a high speed ratio
    + [VECTO-509] Add axle-type (vehicle driven, vehicle non-driven, trailer) to GUI
    + [VECTO-511] Add engine idle speed to Vehicle input form (GUI)
    + [VECTO-510] Write XML reports (manufacturer, customer information) in declaration mode
    + [VECTO-474] new driving cycles for Municipal and Regional Delivery
    + [VECTO-522] step-up ratio for using torque converter in second gear set to 1.85 for busses (still 1.8 for trucks)
    + [VECTO-525] remove info-box with max loading in GUI
    + [VECTO-531] Payload calculation: limit truck payload to the truck's max payload. (earlier versions only limited the total payload of truc + trailer to the total max. payload, i.e. allowed to shifted loading from truck to the trailer)
    + [VECTO-533] allow second driven axle, rdyn is calculated as average of both driven axles
    + [VECTO-537] new Suburban driving cycles
    + [VECTO-541] increase declaration mode PT1 curve to higher speeds (2500 is too low for some engines)



- Bugfixes:
    + [VECTO-462] fix: decision if PTO cycle is simulated
    + [VECTO-473] fix: adapt range for validation of torque converter characteristics
    + [VECTO-464] fix: extrapolation of engine full-load curve gives neg. max. torque. Limit engine speed to n95h
    + [VECTO-480] fix: a_pos in .vsum was less than zero
    + [VECTO-487] fix: Duration of PTO cycle was computed incorrectly if PTO cycle does not start at t=0
    + [VECTO-514] fix: sort entries in .vsum numerically, not lexically
    + [VECTO-516] fix: consider axlegear losses for estimation of acceleration after gearshifts
    + [VECTO-517] fix: valid shift polygon was considered invalid when extended to very high torque ranges
    + [VECTO-424] fix: VectoCore.dll could not be found when the current working directory is different to the directory of the vectocmd.exe
    + [VECTO-425] fix: vectocmd.exe - check if the output is redirected, and skip updating of the progress bar when this is the case
    + [VECTO-426] fix: vectocmd.exe - log errors to STDERR
    + [VECTO-519] fix: computation of n95h fails for a valid full-load curve due to numerical inaccuracy. add tolerance when searching for solutions
    + [VECTO-520] fix: gearashift count in vsum is 0





**VECTO 3.1.2**

***Build 810 (2017-03-21)***

- Improvements:
    + [VECTO-445] Additional columns in vsum file
    + Allow splitting shift losses among multiple simulation intervals
    + Allow coasting overspeed only if vehicle speed > 0
    + Torque converter: better handling of ‘creeping’ situations

- Bugfixes:
    + [VECTO-443] Bugfix in AMT shift strategy: skip gears not working correctly


***Build 796 (2017-03-07)***

- Improvements:
    + [VECTO-405] Adding clutch-losses for AMT/MT gearboxes during drive-off, reduce drive-off distance after stop from 1m to 0.25m, set clutch closing speed (normalized) to 6.5%, changes in clutch model
    + [VECTO-379] Make GUI more tolerant against missing files. Instead of aborting reading the input data the GUI shows a suffix for missing input files
    + [VECTO-411] Allow a traction interruption of 0s for AMT/MT gearboxes
    + [VECTO-408] Gearbox Inertia for AT gearboxes set to 0
    + [VECTO-419] Adapted error messages, added list of errors
    + [VECTO-421,VECTO-439] Added volume-related results to vsum file (volume is computed based on default bodies)
    + [] Energy balance (vsum) and balance of engine power output and power consumers (vmod) level
    + [VECTO-430] AT shift strategy: upshifts may happen too early
    + [VECTO-431] AMT shift strategy always started in first gear due to changes in clutch model
    + [VECTO-433] adapt generic vehicles: use typical WHTC correction factors
    + [VECTO-437] set vehicle speed at clutch-closed to 1.3 m/s
    + [VECTO-436] fix simulation aborts with AT gearbox (neg. braking power, unexpected response, underload)
- Bugfixes:
    + [VECTO-415] Powershift Losses were not considered for AT gearboxes with PowerSplit
    + [VECTO-416] Measured Speed with gear failed when cycle contained parts with eco-roll (computation of next gear failed)
    + [VECTO-428] Sum of timeshares adds up to 100%
    + [VECTO-429] Min Velocity for lookahead coasting was not written to JSON file


**VECTO 3.1.1**

***Build 748 (2017-01-18)***

- Bugfixes:
    + [VECTO-404] Driving Cycle with PTO stopped simulation after first PTO activation


***Build 742 (2017-01-12)***

- Improvements:
    + [VECTO-390, VECTO-400] Adapt engine speed to estimated engine speed after gear shift during traction interruption (double clutching)
    + [VECTO-396, VECTO-388] Add shift losses for AT power shifts
    + [VECTO-389] new gear shift rules for AT gearboxes
    + [VECTO-387] added max input speed for torque converter
    + [VECTO-385] Automatically add generic torque converter data for drag
    + [VECTO-399] Add missions and loadings for vehicle categories 11, 12, and 16 (declaration mode)
    + [VECTO-384] cleanup memory after simulation run
    + [VECTO-394] new option for vectocmd to disable all output
    + [VECTO-392] make the GUI scale depending on the Windows font size
    + [VECTO-391] Gearbox output speed and output torque added to .vmod files
    + [VECTO-386] Gearbox window: disable input fields not applicable for the selected gearbox type
- Bugfixes:
    + [VECTO-401] Computation of n_95h etc. fails if engine’s max torque is constant 0 
Lookup of Airdrag parameters in declaration mode
    + [VECTO-378] Improved file-handling in AAUX module


**VECTO 3.1.0**

***Build 683 (2016-11-14)***

- Bugfixes:
    + [VECTO-375] Fixed bug when braking during slope change from negative to positive values.
    + [VECTO-372] Added check for unusual acceleration/deceleration data which could lead to error when halting.
    + [VECTO-371] Added additional behavior to overcome such situations
    + [VECTO-370] Added additional behavior to overcome such situations
    + [VECTO-369] CrosswindCorrection is now saved and read again from JSON files
    + [VECTO-373] WHTC-Engineering correction factor now correctly read/write in JSON files
    + [VECTO-368] Fixed validation for specific cases when values are intentionally invalid.
    + [VECTO-357] Updated GUI to not show ECO-Roll option to avoid confusion
    + Fixed numerous bugs in AT-ShiftStrategy regarding the Torque Converter
    + Fixed numerous bugs in MeasuredSpeed Mode (and MeasuredSpeed with Gear) in connection with AT-Gearbox and TorqueConverter
    + Fixed a bug when PTO-Cycle was missing
    + Corrected axle loss maps for Generic Vehicles in Declaration Mode to match technical annex
    + Corrected SumFile Cruise Time Share. Added checks that timeshares must add up to 100%

- Improvements:
    + [VECTO-355] Updated documentation, added powertrain schematics in chapter "Simulation Models"
    + [VECTO-374] Check range for Torque Converter speed ratio input data to be at least between 0 and 2.2
    + Updated many error messages to be more explicit about the reason of error
    + Added "Mission Profiles" Directory with driving cycles publicly available in the application root directory.
    + Added "Declaration" directory with the declaration data files in the application root directory.
	+ Added warning when engine inertia is 0
    + Added check that engine speed must not fall below idle speed (even in measured speed mode)
    + Shift curve validation for AT gearboxes: shift curves may now overlap due to different shift logic in AutomaticTransmissions.
    + Updated Crosswind Coefficients for Tractor+Semitrailer


***Build 662 (2016-10-24)***

- Bugfixes:
    + [VECTO-360] Fixed error during startup of VECTO (loading of DLLs).
    + [VECTO-358] Fixed errors during simulation where vehicle unintentionally was driving backwards. Added stricter sanity checks and invariants to avoid such errors. Fixed 1Hz-Filter for ModFiles (distance was wrong under certain circumstances, vehicle seemingly jumping back before halt).
    + [VECTO-361] Fixed classification of vehicles with GVM of exactly 7500kg (Class 1).
    + [VECTO-364] Fixed an error in measured speed mode (run aborts).
    + [VECTO-363] Compute shift polygons in declaration mode now uses correct boundary for full load margin.
    + [VECTO-365] Fixed editing gears in declaration mode

- Improvements:
    + [VECTO-355] User Manual updated (Screenshots, Descriptions, File Formats, Vecto V2 Comments removed).
    + [VECTO-317] Declaration data for Wheel sizes updated
    + [VECTO-359] Simplified code regarding PT1 behavior.
    + [VECTO-323] PTO-Cycle may now be left empty when not used in driving cycle.

***Build 652 (2016-10-14)***

- Main Updates
    + Removed VECTO Core 2.2
    + Refactoring of the User-Interface Backend: loading, saving files and validating user input uses Vecto 3 models
    + AT-Gearbox Model: differentiate between AT gearbox with serial torque converter and AT gearbox using powersplit
    + Numbering of gears with AT gearbox corresponds to mechanical gears, new column TC_locked in .vmod file to indicate if torque converter is active
    + Torque converter gear no longer allowed in input (added by Vecto depending on the AT model)
    + New implementation of torque converter model (analytic solutions)
    + Added PTO option for municipal utility vehicles: PTO idle losses, separate PTO cycle during standstill
    + Added Angledrive Component
    + Option for constant Auxiliary Power Demand in Job-File
    + Normalize x/y values before triangulating Delaunay map (transmission loss-maps, fuel consumption loss map)
    + Additional fuel consumption correction factor in declaration mode: cold/hot balancing factor
    + Added fuel consumption correction factor (WHTC, Cold/Hot balancing, …) in engineering mode
    + Update auxiliaries power demand according to latest whitebook
    + Allow multiple steered axles
    + Adapted engine idle controller (during declutch) – engine speed decreases faster
    + SUM-File: split E_axl_gbx into two columns, E_axl and E_gbx
    + New columns in mod-file: PTO, torque converter
    + Removed full-load curve per gear, only single value MaxTorque
    + Removed rims (dynamic wheel radius depends on wheel type)
    + Fixes in AAUX module: open correct file-browser, save selected files

-------------------------------------------------------------------------------

**VECTO 3.0.4**

***Build 565 (2016-07-19)***

- Bugfixes
    + AAUX HVAC Dialog does not store path to ActuationsMap and SSMSource
    + GUI: check for axle loads in declaration mode renders editing dialog useless
    + Vecto 2.2: Simulation aborts (Vecto terminates) when simulating EngineOnly cycles
    + Vecto 3: Building SimulationRun EngineOnly simulation failed

***Build 544 (2016-06-28)***

- Main Updates
    + New gear shift strategy according to White Book 2016
    + New coasting strategy according to White Book 2016
    + New input parameters (enineering mode) for coasting and gear shift behavior
    + Use SI units in Advanced Auxiliaries Module and compile with strict compiler settings (no implicit casts, etc.)
    + Allow efficiency for transmission losses (in engineering mode)

- Bugfixes
    + Auxiliary TechList not read from JSON input data
    + Improvements in driver strategy
    + Bugfixes in MeasuredSpeed mode

-------------------------------------------------------------------------------

**VECTO 3.0.3**

***Build 537 (2016-06-21)***

- Main Updates
    + Support for Advanced Auxiliaries (Ricardo) in Vecto 3.0.3 and Vecto 2.2
    + Performance improvements
    + Gearshift polygons according to WB 2016
    + Revision of SUM-data file, changed order of columns, changed column headers
- Bugfixes
    + Delaunay Maps: additional check for duplicate input points
    + Creation of PDF Report when running multiple jobs at once
    + Sanity checks for gear shift lines
    + Improvements DriverStrategy: handling special cases

***Build 495 (2016-05-10)***

- Main Updates
    + Support for Advanced Auxiliaries (Ricardo) in Vecto 3.0.3 and Vecto 2.2
    + Performance improvements
    + Gearshift polygons according to WB 2016
    + Revision of SUM-data file, changed order of columns, changed column headers
- Bugfixes
    + Delaunay Maps: additional check for duplicate input points
    + Creation of PDF Report when running multiple jobs at once
    + Sanity checks for gear shift lines
    + Improvements DriverStrategy: handling special cases

-------------------------------------------------------------------------------

**VECTO 3.0.2**

***Build 466 (2016-04-11)***

- Bugfix: calculation of CO2 consumption based on FC-Final (instead of FC-map)
- Bugfix: acceleration in .vmod was 0 in certain cases (error in output)
- Bugfix: syncronized access to cycle cache (declaration)

***Build 448 (2016-03-24)***

- Bugfix: set WHTC factors to a valid default value in engineering mode
- Bugfix: first page of declaration report was missing
- fixed inconsistencies in user manual
- Bugfix: better error message roll resistance calculation could not be calculated
- Bugfix: measured speed now calculates distance correctly
- Bugfix: measured speed fills missing moddata columns (acc, dist, grad)
- Bugfix: better error message when driving cycle is missing.
- Bugfix: vectocmd errormsg when writing progress

***Build 434 (2016-03-10)***

- New simulation modes:
    + Measured Speed
    + Measured Speed with Gear
    + Pwheel (SiCo)
- Adaptations of powertrain components architecture
    + Move wheels inertia from vehicle to wheels
    + Auxiliaries no longer connected via clutch to the engine but via a separate port
    + Engine checks overload of gearbox and engine overload
- Fixed some driving behavior related issues in VectoCore:
    + When the vehicle comes to a halt during gear shift, instead of aborting the cycle, it tries to drive away again with an appropriate gear.
- [ModData Format](#modal-results-.vmod) changed for better information and clarity
- Entries in the sum-file are sorted in the same way as in Vecto 2.2
- In engineering mode the execution mode (distance-based, time-based measured speed, time-based measured speed with gear, engine only) are detected based on the cycle
- Added validation of input values
- Gravity constant set to 9.80665 (NIST standard acceleration for gravity)
- Improved input data handling: sort input values of full-load curves (engine, gbx, retarder)
- Better Integration of VectoCore into GUI (Notifications and Messages)
- Speed dependent cross-wind correction (vcdv) and v_air/beta cross-wind correction (vcdb) impemented
- For all calculations the averaged values of the current simulation step are used for interpolations in loss-maps.
- Allow extrapolation of loss maps in engineering mode (warnings)
- Refactoring of input data handling: separate InputDataProvider interfaces for model data
- Refactoring of result handling: separate result container and output writer
- New Long-Haul driving cycle included
- User Manual updated for VECTO V3.x
- Fix: sparse representation of declaration cycles had some missing entries
- Bugfix: error in computation of engine's preferred speed
- Bugfix: wrong vehicle class lookup
- Bugfix: duplicate entries in intersected full-load curves
- Bugfix: retarder takes the retarder ratio into account for lossmap lookup
- Bugfix: use unique identifier for jobs in job list
- Bugfix: error in triagulation of fuel consumption map






